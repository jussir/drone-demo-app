const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify({ foo: 'bar reply' }))
})

app.listen(port, () => {
  console.log(`listening at http://localhost:${port}`)
})

module.exports = app;