const test = require('ava');
const request = require('supertest')

const app = require('./src/main')

test('foo', async t => {
  const res = await request(app).get('/');

  t.is(res.statusCode, 200);
  t.deepEqual(res.body, { foo: 'bar reply' });
  t.pass();
});

test('bar', async t => {
  const bar = Promise.resolve('bar');
  t.is(await bar, 'bar');
});
